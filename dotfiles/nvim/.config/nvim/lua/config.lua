require("settings")
require("colorscheme")
require("keymappings")
require("autocommands")
require("commands")

require("lsp")

require("b-cmp")

vim.cmd("source ~/.config/nvim/vimscript/functions.vim")
