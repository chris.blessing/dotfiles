local M = {}

M.setup = function()
	require("colorizer").setup({
		"css",
		"scss",
	})
end

return M
