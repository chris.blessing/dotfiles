config.load_autoconfig(False)

c.editor.command = ["st", "nvim", "{file}"]
c.content.javascript.can_access_clipboard = True
